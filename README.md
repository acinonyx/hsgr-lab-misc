# HSGR Lab Miscellaneous repository #

This repository contains miscellaneous source code that does not fit into any other repository.

## HSGR Lab Electrical Plan ##

An electrical plan for [QElectroTech](https://qelectrotech.org/) of HSGR Lab is available.
The plan includes:

  * riser diagrams, overlaid on the top view of the lab floors
  * panel diagrams

The plan is created with version 0.80 of QElectroTech.
